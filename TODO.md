1. use an inert (_) value for special ignorable bindings
1. devise a good condition system
1. write `pr` (print)
1. for `pr` a generic mechanism is needed
1. write `read`
1. if reading from a file use `meta`
