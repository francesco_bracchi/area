## config
OPTIMIZE_LEVEL		= 	3
DEBUG_LEVEL     	= 	0
PROFILE			= 	0

## stuff
SCHEME			= 	scheme
RM			=	rm -f
TIME			= 	time -p

## params
OPTIMIZE_PARAM 		= 	(optimize-level $(OPTIMIZE_LEVEL))
DEBUG_PARAM 		=	(debug-level $(DEBUG_LEVEL))

ifeq ($(PROFILE), 1)
PROFILE_PARAM  		= 	(compile-profile (quote source))
else
PROFILE_PARAM 		=
endif

COMPILE_IMP_PARAM 	=	(compile-imported-libraries \#t)
GENERATE_WPO_PARAM 	= 	(generate-wpo-files \#t)
INSPECT_PARAM   	= 	(generate-inspector-information \#t)
COMPILE_PARAMS		= 	$(OPTIMIZE_PARAM) $(DEBUG_PARAM) $(PROFILE_PARAM) $(INSPECT_PARAM) $(COMPILE_IMP_PARAM) $(GENERATE_WPO_PARAM)

LIBDIR          	= 	src/scheme
SCHEME_OPTS		= 	-q --libdirs $(LIBDIR)

LIBS            	=	$(LIBDIR)/area/env.so $(LIBDIR)/area/cekm.so $(LIBDIR)/area/prim.so

%.so: %.ss
	echo '(parameterize ($(COMPILE_PARAMS)) (compile-file "$<" "$@"))' | $(SCHEME) $(SCHEME_OPTS)

clean:
	find src -name "*.so" | xargs $(RM)
	find src -name "*.boot" | xargs $(RM)
	find src -name "*.wpo" | xargs $(RM)
	$(RM) fib
	$(RM) repl

fib:
	echo '(parameterize ($(COMPILE_PARAMS)) (compile-program "$(LIBDIR)/fib.ss") (compile-whole-program "$(LIBDIR)/fib.wpo" "fib"))' | $(SCHEME) $(SCHEME_OPTS)

repl:
	echo '(parameterize ($(COMPILE_PARAMS)) (compile-program "$(LIBDIR)/repl.ss") (compile-whole-program "$(LIBDIR)/repl.wpo" "repl"))' | $(SCHEME) $(SCHEME_OPTS)

@PHONY: compile clean
