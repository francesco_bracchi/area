(library (area scheduler)
  (export make-scheduler schedule)
  (import (chezscheme)
          (prefix (area queue) queue/)
          (prefix (area cekm) cekm/))

  ;; todo try out work stealing?

  (define-record-type (scheduler make-scheduler-aux scheduler?)
    (fields (immutable size)
            (immutable workers)
            (immutable running)))

  (define (make-scheduler size)
    (let ((queue (queue/new)))
      (make-scheduler-aux
       size
       (map (partial make-worker queue) (ran size))
       queue)))

  (define (partial f . as)
    (lambda bs (apply f (append as bs))))

  (define (ran n)
    (let ran ((j n) (ps '()))
      (if (<= j 0) ps
          (ran (- j 1) (cons j ps)))))

  (define (forever q)
    (let forever ()
      ((queue/pop! q))
      (forever)))

  (define (make-worker q id)
    (fork-thread (lambda () (forever q))))

  (define (schedule scheduler thunk)
    (queue/push! (scheduler-running scheduler) thunk))

  )
