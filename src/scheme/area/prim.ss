(library (area prim)
  (export initial-env)
  (import (chezscheme)
          (area prim utils)
          (prefix (area env) env/)
          (prefix (area cekm) cekm/)
          (prefix (area prim io) io/)
          (prefix (area prim meta) meta/)
          (prefix (area prim cell) cell/)
          (prefix (area prim load) load/)
          (prefix (area prim capsule) capsule/))

  (define initial-env (env/empty))

  (define (add key val)
    (env/def initial-env key val))

  (add 'load load/area-load)

  (add 'do seq)

  (add 'true #t)

  (add 'false #f)

  (add 'nil '())

  (add 'cons (lift cons))

  (add 'car (lift (lambda (e) (if (null? e) '() (car e)))))

  (add 'cdr (lift (lambda (e) (if (null? e) '() (cdr e)))))

  ;; (add 'car! (lift set-car!))

  ;; (add 'cdr! (lift set-cdr!))

  (add 'pair? (lift pair?))

  (add 'nil? (lift null?))

  (add 'sym? (lift symbol?))

  (add 'if
       (fx (vs e k m t)
           (cekm/eval (car vs) e (cons (cekm/make-cnd e (cadr vs) (caddr vs)) k) m t)))

  (add 'fx
       (fx (vs e k m t)
           (cekm/eval (cekm/make-fexp e (car vs) (cadr vs) (cons seq (cddr vs))) e k m t)))

  (add 'def
       (fx (vs e k m t)
           (cekm/eval (cadr vs) e (cons (cekm/make-def e (car vs)) k) m t)))

  (add 'set!
       (fx (vs e k m t)
           (cekm/eval (cadr vs) e (cons (cekm/make-set e (car vs)) k) m t)))

  (add 'eval
       (fn (vs e k m t)
           (cekm/eval (cadr vs) (car vs) k m t)))

  (add 'env
       (fn (vs e k m t)
           (cekm/continue e e k m t)))

  (add 'empty-env
       (fn (vs e k m t)
           (cekm/continue (env/empty) e k m t)))

  (add 'reset
       (fx (vs e k m t)
           (cekm/eval (cons seq vs) e '() (cons k m) t)))


  (add 'shift
       (fx (vs e k m t)
               (let*((nn (car vs))
                     (kk (fn (vs1 e1 k1 m1 t1)
                             (cekm/continue (car vs1) e1 k (cons k1 m1) t1)))
                     (bd (if (null? (cddr vs)) (cadr vs) (cons seq (cdr vs))))
                     (e1 (env/push e)))
                 (env/def e1 nn kk)
                 (cekm/eval bd e1 (car m) (cdr m) t))))


  (add 'env-push
       (lift env/push))

  (add 'wrap (lift cekm/make-wrapper))

  (add 'unwrap (lift cekm/wrapper-val))

  (add 'wrap? (lift cekm/wrapper?))

  (add 'fexp? (lift cekm/fexp?))

  (add 'err
       (fn (vs e k m t)
           (cekm/make-fail (list '() e k m) (car vs) (cadr vs))))

  (add 'par par)

  (add 'builtin? (list cekm/builtin?))

  (add 'num? (lift number?))

  (add '+ (lift +))

  (add '- (lift -))

  (add '* (lift *))

  (add '/ (lift /))

  (add 'div (lift div))

  (add 'mod (lift mod))

  (add '= (lift eq?))

  (add 'not (lift not))

  (add '>= (lift >=))

  (add '<= (lift <=))

  (add '< (lift <))

  (add '> (lift >))

  ;; todo remove
  (add 'pr (lift pr))

  (add 'stdin (lift current-input-port))

  (add 'stdout (lift current-output-port))

  (add 'stderr (list current-error-port))

  (add 'time (lift current-time))

  (add 'time-diff (lift time-difference))

  (add 'io/eof? (list eof-object?))

  (add 'io/read io/read-exp)

  (add 'io/in io/in-file)

  (add 'io/out io/out-file)

  (add 'io/close io/close)

  (add 'io/stdin (lift standard-input-port))

  (add 'io/stdout (lift standard-output-port))

  (add 'io/stderr (lift standard-error-port))

  (add 'io/read-bin io/read-bin)

  (add 'meta meta/get)

  (add 'w/meta meta/with)

  (add 'tmp/read (lift read))

  (add 'new-type capsule/new-type)

  (add 'type (lift capsule/area-type))

  (add 'halt (lift exit))

  ;; (add 'cell cell/new)

  ;; (add 'cell-set cell/set)

  ;; (add 'cell-deref cell/deref)

  )
