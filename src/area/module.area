;;; -*- area_mode -*-

(def (really-make-struct struct-type unstruct)
  (new-type))

(def (make-interface interface-type interface-symbols)
  (new-type))

(def (make-package package-type package-function)
  (new-type))

(defn make-struct
    (e iface)
  (/> iface
      interface-symbols
      (map (fn (x) (cons x x)))
      (list e iface)
      really-make-struct))

(defn struct-env
    (s)
  (when (struct? s)
    (car (unstruct s))))

(defn struct-interface
    (s)
  (when (struct? s)
    (cdr (unstruct s))))

(defn eval-in
    (s e)
  (eval (struct-env e) e))

(defn modify
    (s fun)
  (let ((e i m) (unstruct s))
    (really-make-struct e i (fun m))))

(defx prefix
    _ (p)
  (part map (fn ((o . d)) (cons o (sym (conc p d))))))

(defx only
    _ es
  (let (exp? (part (flip in?) es))
    (part filter (comp exp? cdr))))

(defn build-env
    (e bs)
  (def e1 (empty-env))
  (each (fn (b) ((eval e b) e1)) bs)
  e1)

(defx structs
    e (is . bs)
  (map (part make-struct (build-env e bs)) is))

(defx struct
    e (i . bs)
  (make-struct (build-env e bs) i))

(defx defstructs
    e (ps . bs)
  (let (ns (evens ps) is (odds ps))
    (eval e
      (list def ns (list* structs is bs)))))

(defx defstruct
    e (n i . bs)
  (eval e
    (insp (list* defstructs (list n) (list i) bs))))

(defx packages
    e (fs is . bs)
  (make-package
   (eval e
     (list fn fs (list* structs is bs)))))

(defx package
    e (fs i . bs)
  (make-package
   (eval e
     (list fn fs (list* struct i bs)))))

(defx defpackage
    e (n fs i . bs)
  (eval e
    (list def n (list* package fs i bs))))

(defx defpackages
    e (n fs is . bs)
  (eval e
    (list def n (list* packages fs is bs))))

(defx open-one
    ed (s)
  (let ((eo i m) (unstruct s))
    (each (fn ((o . d))
            (eval ed
              (list def d (eval eo o))))
          m)))

(defx open
    ed ss
  (each (fn (s)
          (eval e (list open-one s)))
        ss))

(defx files
    e fs
  (each (fn (f)
          (eval e
            (list load f)))
        fs))

(defx inline
    e xs
  (each (part eval e) xs))

(defn struct?
    (s)
  (= interface-type (type s)))

(defn interface?
    (i)
  (= struct-type (type i)))

(defn package?
    (p)
  (= package-type (type p)))

(defx export
    _e as
  (make-interface as))

(defn union2
    ij
  (/> ij
      (map interface-symbols)
      (apply conc)
      uniq
      (make-interface)))

(defn union
    is
  (reduce union2 nil is))

(defn reload!
    (s)
   nil)

(def area.modules.iface
  (export structs
          struct
          defstructs
          defstruct
          packages
          package
          defpackages
          defpackage
          open
          files
          struct?
          interface?
          package?
          export))

(def area.modules.meta.iface
  (export struct-env
          struct-interface
          eval-in))

(def area.modules.all-modules.iface
  (export all-modules
          add-module!
          current-module
          eval-in-current-module))

(def area.core.list.iface
  (export nil
          cons
          car
          cdr
          fold
          foldr
          rev
          ran
          caar
          cdar
          cadr
          cddr
          caddr
          cdddr
          cadddr
          cddddr
          last
          but-last
          all?
          some?
          conc
          map
          each
          filter
          remove
          find
          in?
          uniq
          evens
          odds
          len
          drop
          take
          nth))

(def area.core.stack.iface
  (export push! pop! peek))

(def area.core.basic.iface
  (export q
          id
          list
          apply
          list*
          fn
          defx
          defn
          cond
          when
          when-not
          flip
          comp
          always
          and
          or
          part
          let
          loop
          inc
          dec
          zero?
          pos?
          neg?
          />
          do1
          do
          true
          false
          pair?
          nil?
          sym?
          if
          fx
          def
          eval
          env
          empty-env
          reset
          shift
          env-push
          wrap
          unwrap
          wrap?
          fexp?
          err
          builtin?
          num?
          +
          -
          *
          /
          div
          mod
          =
          mot
          >=
          <=
          <
          >
          pr
          time
          meta
          w/meta
          new-type
          halt))

(def area.core.iface
  (union area.core.basic.iface
         area.core.list.iface
         area.core.stack.iface))

(def area.modules
  (make-struct (env) area.modules.iface))

(def area.modules.meta
  (make-struct (env) (list area.modules.meta.iface)))

(def area.modules.all-modules
  (make-struct (env) area.modules.all-modulers.iface))
