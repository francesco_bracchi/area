;; (open area.list.base)

(def q
  (fx _ (p) p))

(def id
  (wrap (fx _ (v) v)))

(def apply
  (wrap
   (fx _ (o cs . es)
     (eval (if (nil? es) (env) (car es))
       (cons (unwrap o) cs)))))

(def fn
  (fx e (as . bs)
    (wrap
     (eval e
       (list* fx (q ign) as bs)))))

(def defx
  (fx e (n e1 as . bs)
    (eval e
      (list def n (list* fx e1 as bs)))))

(def defn
  (fx e (n as . bs)
    (eval e
      (list def n (list* fn as bs)))))

(defn evens
    (cs)
  (if (nil? cs) nil
      (cons (car cs) (odds (cdr cs)))))

(defn odds
    (cs)
  (if (nil? cs) nil
      (evens (cdr cs))))

(defx loop
    e (n bs . xs)
  (def e1 (env-push e))
  (eval e1 (list* defn n (evens bs) xs))
  (eval e1 (list* n (odds bs))))

(defx cond
    e cs
  (loop cond (cs cs)
       (if (nil? cs) nil
           (if (nil? (cdr cs))
               (eval e (car cs))
               (if (eval e (car cs))
                   (eval e (car (cdr cs)))
                   (cond (cdr (cdr cs))))))))
(defx when
    e (t . as)
  (eval e
    (list cond t (list* do as))))

(defx when-not
    e (t . as)
  (eval e
    (list* when (list not t) as)))

(defn flip
    (f)
  (fn (a b . as) (apply f (list* b a as))))

(defn comp
    fs
  (cond
    (nil? fs) id
    (nil? (cdr fs)) (car fs)
    (nil? (cdr (cdr fs)))
    (do (def f (car fs))
        (def g (car (cdr fs)))
      (fn (a) (f (g a))))
    (fold comp (car fs) (cdr fs))))

(defn always
    (v)
  (fn _ v))

(defx and
    e bs
  (all? (part eval e) bs))

(defx or
    e bs
  (some? (part eval e) bs))

(defn conc2
    (as bs)
  (if (nil? as) bs
      (cons (car as) (conc2 (cdr as) bs))))

(defn part
    (f . cs)
  (fn bs (apply f (conc2 cs bs))))

(defx let
  e (bs . xs)
  (def e1 (env-push e))
  (each (fn (k v) (eval e1 (list def k v)))
        (evens bs)
        (odds bs))
  (eval e1 (list* do xs)))

(defx do1
    e (b . bs)
  (def r1 (eval e b))
  (each (part eval e) bs)
  r1)

(defn pipe
    (a c)
  (cond
    (pair? c) (conc2 c (list a))
    (list c a)))

(defx />
    e (c . cs)
  (eval e (fold pipe c cs)))

(defx swap
    e (p q)
  (let (x (eval e q))
    (eval e (list set! q p))
    (eval e (list set! p x))))

;; numeric stuff
(def inc
  (part + 1))

(def dec
  (part (flip -) 1))

(def zero?
  (part = 0))

(def pos?
  (part < 0))

(def neg?
  (part > 0))

(def not-pos?
  (part >= 0))

(def not-neg?
  (part <= 0))
